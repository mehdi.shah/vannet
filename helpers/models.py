from django.db import models


class TrackingModel(models.Model):
    created_by = models.CharField(max_length=255, blank=False, null=False)
    updated_by = models.CharField(max_length=255, blank=False, null=False)
    deleted_at = models.DateTimeField(null=True)
    deleted_by = models.CharField(
        max_length=255, default="None", blank=False, null=False)
    is_deleted = models.BooleanField(default=False)
    is_blocked = models.BooleanField(default=False)

    class Meta:
        abstract = True
        ordering = ('deleted_at',)
