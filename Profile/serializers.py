from rest_framework import fields
from rest_framework import serializers

from Profile.models import Profile


class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = ('id', 'phone', 'email', 'fullname', 'username', 'image',
                  'birth_date', 'wallet_id', 'referrer_code', 'invite_code')
        read_only_fields = ('id', 'wallet_id', 'referrer_code', 'phone')
