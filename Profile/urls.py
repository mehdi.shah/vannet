from django.urls import path

from Profile import views


urlpatterns = [
    path('', views.ProfileView.as_view(), name='profile'),
    path("edit-profile/<int:pk>/",
         views.ProfileUpdateView.as_view(), name="edit-profile"),
]
