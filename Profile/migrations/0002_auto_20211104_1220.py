# Generated by Django 3.2.9 on 2021-11-04 12:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Profile', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='birth_date',
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='profile',
            name='phone',
            field=models.CharField(blank=True, default='<django.db.models.query_utils.DeferredAttribute object at 0x7f037a89e820>', max_length=17, null=True),
        ),
    ]
