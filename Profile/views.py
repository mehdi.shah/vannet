from django.shortcuts import render

from rest_framework import generics
from rest_framework import permissions

from Profile.models import Profile
from Profile.serializers import ProfileSerializer
from Profile.permissions import IsOwner


class ProfileView(generics.ListCreateAPIView):
    serializer_class = ProfileSerializer
    queryset = Profile.objects.all().order_by("-updated_date")
    permission_classes = (IsOwner, permissions.IsAuthenticated,)

    def perform_create(self, serializer):
        return serializer.save(owner=self.request.user)

    def get_queryset(self):
        return self.queryset.filter(owner=self.request.user)


class ProfileUpdateView(generics.UpdateAPIView):
    serializer_class = ProfileSerializer
    queryset = Profile.objects.all().order_by("-updated_date")
    permission_classes = (IsOwner, permissions.IsAuthenticated,)

    def get_queryset(self):
        return self.queryset.filter(owner=self.request.user)
