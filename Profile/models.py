from django.db import models
from django.utils.translation import gettext_lazy as _
from django.utils import timezone

from authentication.models import User

from helpers.models import TrackingModel

import json


class Profile(TrackingModel):
    email = models.EmailField(
        max_length=128, blank=False, null=False, unique=True)
    fullname = models.CharField(max_length=68, blank=False, null=False)
    username = models.CharField(
        _('username'),
        max_length=150,
        unique=True,
        help_text=_(
            'Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.'),
        error_messages={
            'unique': _("A user with that username already exists."),
        },
    )
    phone = models.CharField(default=str(User.phone),
                             max_length=17, blank=True, null=True)
    image = models.ImageField(
        _('image'), default="", upload_to="store_user_image", null=True, blank=True)
    birth_date = models.DateField(blank=True, null=True)
    wallet_id = models.CharField(max_length=68, blank=True, null=True)
    referrer_code = models.CharField(max_length=68, blank=True, null=True)
    invite_code = models.CharField(max_length=68, blank=True, null=True)
    push_token = models.CharField(max_length=128, blank=True, null=True)
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)
    updated_date = models.DateTimeField(auto_now=True)

    owner = models.ForeignKey(to=User, on_delete=models.CASCADE)

    def __str__(self):
        return self.email
