from django.urls import path

from authentication import views

from rest_framework_simplejwt.views import (TokenRefreshView)


urlpatterns = [
    path('<phone>/', views.LoginView.as_view(), name='Login'),
    path("token-refresh/", TokenRefreshView.as_view(), name="token_refresh"),
]
