from django.contrib import admin
from django.contrib.auth.models import Group

from authentication.models import User


class UserAdmin(admin.ModelAdmin):
    list_display = ['phone', 'updated_date', 'date_joined']
    list_filter = ('phone', 'date_joined', 'updated_date')

    search_fields = ('phone', 'id')
    ordering = ('phone', 'date_joined', 'updated_date', 'id')


admin.site.register(User, UserAdmin)
admin.site.unregister(Group)
