from django.conf import settings
from django.contrib import auth
from django.utils.http import urlsafe_base64_decode, urlsafe_base64_encode
from django.utils.encoding import smart_str, force_str, smart_bytes, DjangoUnicodeDecodeError

from rest_framework import serializers
from rest_framework.exceptions import AuthenticationFailed

from authentication.models import User

from datetime import datetime, timedelta

import jwt


class LoginSerializer(serializers.ModelSerializer):
    tokens = serializers.CharField(max_length=128, min_length=6)
    phone = serializers.CharField(max_length=17, min_length=4)
    otp = serializers.IntegerField()

    class Meta:
        model = User
        fields = ['tokens', 'phone', 'otp']
        read_only_fields = ['tokens', ]
