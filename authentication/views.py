from django.shortcuts import render
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth import models, tokens

from rest_framework import permissions
from rest_framework.response import Response
from rest_framework.views import APIView

from authentication.models import User
from authentication.serializers import LoginSerializer

from twilio.rest import Client
from datetime import datetime, timedelta

import pyotp
import base64
import twilio
import random


account_sid = 'ACd96e4f2268eb1dce75db904f408071b8'
auth_token = 'bb1dd8c8896dc08d0e33cc4c6428e043'
client = Client(account_sid, auth_token)

# This class returns the string needed to generate the key
RandomKey = {'VENEZUELA': 'CARACAS', 'CANADA': 'OTTAWA'}


class generateKey:
    @staticmethod
    def returnValue(phone):
        return str(phone) + str(datetime.date(datetime.now())) + random.choice(list(RandomKey.values()))


class LoginView(APIView):
    permission_classes = (permissions.AllowAny,)
    # Get to Create a call for OTP

    @staticmethod
    def get(request, phone):

        try:
            # if Mobile already exists the take this else create New One
            Mobile = User.objects.get(phone=phone)
        except ObjectDoesNotExist:
            User.objects.create(
                phone=phone,
            )
            Mobile = User.objects.get(
                phone=phone)  # user Newly created Model
        Mobile.counter += 1  # Update Counter At every Call
        Mobile.save()  # Save the data
        keygen = generateKey()
        key = base64.b32encode(keygen.returnValue(
            phone).encode())  # Key is generated
        OTP = pyotp.HOTP(key)  # HOTP Model for OTP is created
        print(OTP.at(Mobile.counter))
        # Using Multi-Threading send the OTP Using Messaging Services like Twilio or Fast2sms
        # Just for demonstration
        """message = client.messages.create(
            body='Hello, Your Secure Device OTP is - ' +
            str(OTP.at(Mobile.counter)),
            from_="+43 670 3091447",
            to=Mobile.phone
        )
        print(message.sid)"""
        return Response({"otp": OTP.at(Mobile.counter)}, status=200)

    # This Method verifies the OTP
    @staticmethod
    def post(request, phone):
        try:
            Mobile = User.objects.get(phone=phone)
        except ObjectDoesNotExist:
            return Response("User does not exist", status=404)  # False Call

        keygen = generateKey()
        key = base64.b32encode(keygen.returnValue(
            phone).encode())  # Generating Key
        OTP = pyotp.HOTP(key)  # HOTP Model
        if OTP.verify(request.data["otp"], Mobile.counter):  # Verifying the OTP
            Mobile.isVerified = True
            Mobile.active = True
            Mobile.otp = OTP.at(Mobile.counter)
            Mobile.save()
            user = User.objects.get(phone=phone)
            serializer = LoginSerializer(user)
            return Response({"message": "You are authorized", "data": serializer.data}, status=200)
        return Response("OTP is wrong", status=400)
